import 'reflect-metadata'
import { createLogger, format, transports } from 'winston'
import { createConnection } from 'typeorm'

const logger = createLogger({
    level: 'debug',
    format: format.printf(message => 
        `${message.level.toUpperCase()} ${new Date().toISOString()} ${message.message}` 
    ) ,
    transports: [
        new transports.Console(),
    ],
})

export function info(message: unknown): void {
    logger.info(message)
}
export function error(message: unknown): void {
    logger.error(message)
}

export async function init(): Promise<void> {
    await createConnection()
}

