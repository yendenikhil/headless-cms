import express from 'express'
import { router as postsRouter } from './posts'
import { info, init } from './utils'
import morgan from 'morgan'
import helmet from 'helmet'
import cors from 'cors'

const port = process.env.PORT ?? 3000

const app = express() 

app.use(morgan('tiny'))
app.use(express.json())
app.use(helmet())
app.use(cors())

app.use('/posts', postsRouter)

app.get('/', (req, res) => {
    res.end('Hello, World')
})

init().then(() => {
    app.listen(port, () => {
        info(`App started on *: ${port}`)
    })
})
