import { Router } from 'express'
import { Post } from './post'

export  const router = Router() 

router.get('/', async (req, res) => {
    res.json(await Post.find())
})

router.get('/:slug', async (req, res) => {
    const post = await Post.findOne({ slug: req.params.slug })
    if (post) {
        res.json(post)
    } else {
        res.status(404)
        res.end()
    }
    
})

router.post('/', async (req, res) => {
    try {
        const post = await Post.of(req.body)
        res.json(await post.save())
    } catch(err) {
        res.status(err.status ?? 500)
        res.json({ 
            message: err.message ?? 'Cannot create post!'
        })
    }
})

router.delete('/:slug', async (req, res) => {
    await Post.delete({ slug: req.params.slug})
    res.end()
})

router.patch('/:id', async (req, res) => {
    try {
        res.json(await Post.updateme(req.body))    
    } catch (err) {
        res.status(err.status ?? 500)
        res.json({ 
            message: err.message ?? 'Cannot update post!'
        })
    }
    
    
})
