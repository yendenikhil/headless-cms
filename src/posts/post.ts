import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, Index } from 'typeorm'
import { nanoid } from 'nanoid'
import * as yup from 'yup'
import marked from 'marked'
import hljs from 'highlight.js'

marked.setOptions({
    smartLists: true,
    xhtml: true,
    highlight(code, language) {
        const validLanguage = hljs.getLanguage(language) ? language : 'plaintext'
        return hljs.highlight(validLanguage, code).value
    }
})

const schema = yup.object().shape({
    slug: yup.string().optional().matches(/[\w-]*/i),
    isPage: yup.boolean().optional(),
    title: yup.string().required(),
    markdown: yup.string().required()
})

async function generateSlug(size = 10): Promise<string> {
    const slug = nanoid(size).toLowerCase()
    const post = await Post.findOne({ slug })
    if (post) {
        return generateSlug(size)
    } else {
        return slug
    }
}

@Entity()
export class Post extends BaseEntity {
    @PrimaryGeneratedColumn()
    id?: number
    @Column()
    @Index({ unique: true })
    slug: string
    @Column()
    title: string
    @Column()
    markdown: string 
    @Column({ nullable: true })
    html?: string 
    @Column({ nullable: true })
    excerpt?: string 
    @Column()
    isPage: boolean
    @Column()
     createdAt?: Date
    @Column()
    updatedAt?: Date
    @Column({ nullable: true })
    publishedAt?: Date
    constructor(title:string, slug:string, markdown:string, html: string, isPage = false) {
        super()
        this.title = title
        this.markdown = markdown
        this.html = html
        this.isPage = isPage ?? false
        this.slug = slug
        this.createdAt = new Date()    
        this.updatedAt = new Date()
    }

    static async of(input: Post): Promise<Post> {
        const valid = await schema.isValid(input)
        if (valid) {
            input.slug = input.slug ?? await generateSlug(3)
            input.slug = input.slug.toLowerCase()
            input.isPage = input.isPage ?? false
            input.html = marked(input.markdown)
            const {title, slug, markdown, html, isPage = false} = input
            return new Post(title, slug, markdown, html, isPage)
        }
        else {
            throw Error('Invalid Post properties. ')
        }    
    }

    static async updateme(input: Post): Promise<Post> {
        const valid = await schema.validate(input)
        if (valid) {
            const post = await Post.findOne({slug: input.slug})
            if (post) {
                if (post.title !== input.title) {
                    post.title = input.title 
                } if (post.markdown !== input.markdown) {
                    post.markdown = input.markdown
                    post.html = marked(input.markdown)
                }
                post.updatedAt = new Date()
                return await post.save()
            } else {
                throw Error(`Post with slug ${input.slug} found.`)
            }
        } else {
            throw Error('Invalid Post properties. ')
        } 
    }
}

