# Headless CMS

One more attempt for creating headless content management system. 

# Features

# Types of Content

The content is 
  - post 
  - page

# Post 
This is about content which will be dynamically categorized and shown in different contexts.

# Page
The page is static content with its own slug. This is not categorized.

## Structure

- uuid
- id
- slug
- html
- excerpt
- markdown
- tags
- isPage
- createdAt
- updatedAt
- publishedAt



# TODO

- post workflow
- users / roles 
- authentication

# Inspiration
- [Ghost](https://ghost.org)